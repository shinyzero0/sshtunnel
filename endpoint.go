package sshtunnel

import (
	"strings"
)

type Endpoint struct {
	Proto string
	Host  string
	User  string
}

// NewEndpoint creates an Endpoint from a string that contains a user, host and
// port. Both User and Port are optional (depending on context). The host can
// be a domain name, IPv4 address or IPv6 address. If it's an IPv6, it must be
// enclosed in square brackets
func NewEndpoint(s string) (*Endpoint, error) {
	endpoint := &Endpoint{
		Host: s,
	}

	if parts := strings.Split(endpoint.Host, "@"); len(parts) > 1 {
		endpoint.User = parts[0]
		endpoint.Host = parts[1]
	}

	return endpoint, nil
}

func (endpoint *Endpoint) String() string {
	return endpoint.Host
}
